﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoApp.Models.View
{
    public class HomeViewModel
    {
        public IEnumerable<Movie> ThreeMostRecentMovies { get; set; }
    }
}